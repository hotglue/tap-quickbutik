"""Stream type classes for tap-quickbutik."""

from pathlib import Path
from re import I
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th
from sqlalchemy import null  # JSON Schema typing helpers
import requests
from tap_quickbutik.client import hotglueStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class ProductStream(hotglueStream):
    """Define custom stream."""
    name = "product"
    path = "/products"
    records_jsonpath = "$.[*]"
    primary_keys = ["id"]
    replication_key = None
    # for last page
    product_page = False
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(

        th.Property("id", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("hasVariants", th.StringType),
        th.Property("title", th.StringType),
        th.Property("visible", th.StringType),
        th.Property("qty", th.StringType),
        th.Property("qty_location", th.StringType),
        th.Property("price", th.StringType),
        th.Property("purchase_price", th.StringType),
        th.Property("before_price", th.StringType),
        th.Property("description", th.StringType),
        th.Property("seo_title", th.StringType),
        th.Property("seo_description", th.StringType),
        th.Property("weight", th.StringType),
        th.Property("gtin", th.StringType),
        th.Property("supplier_name", th.StringType),
        th.Property("supplier_sku", th.StringType),
        th.Property("tax_rate", th.StringType),
        th.Property("date_modified", th.StringType),    
        th.Property("datafield_1", th.StringType),    
        th.Property("datafield_2", th.StringType),    
        th.Property("datafield_3", th.StringType),    
        th.Property("maincat_id", th.StringType),    
        th.Property("allow_minusqty", th.StringType),    
        th.Property("uri", th.StringType)  
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        if "id" in record.keys():
            return {
            "product_parent_id": record["id"],
            }
        else:
            return {}


class ProductDetailStream(hotglueStream):
    """Define custom stream."""
    name = "product_detail"
    path = "/products?product_id={product_parent_id}&include_details=True"
    records_jsonpath = "$[*]"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = ProductStream
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(

        th.Property("id", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("hasVariants", th.StringType),
        th.Property("title", th.StringType),
        th.Property("visible", th.StringType),
        th.Property("qty", th.StringType),
        th.Property("qty_location", th.StringType),
        th.Property("price", th.StringType),
        th.Property("purchase_price", th.StringType),
        th.Property("before_price", th.StringType),
        th.Property("description", th.StringType),
        th.Property("seo_title", th.StringType),
        th.Property("seo_description", th.StringType),
        th.Property("weight", th.StringType),
        th.Property("gtin", th.StringType),
        th.Property("supplier_name", th.StringType),
        th.Property("supplier_sku", th.StringType),
        th.Property("tax_rate", th.StringType),
        th.Property("date_modified", th.StringType),
        th.Property("images", th.ObjectType(
            th.Property("1", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("2", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("3", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("4", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("5", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("6", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("7", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("8", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("9", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
            th.Property("10", th.ObjectType(
                th.Property("image", th.StringType),
                th.Property("alttext", th.StringType),
                th.Property("id", th.StringType),
            )),
        )),
        th.Property("disable_minusqty", th.BooleanType),
        th.Property("category_id", th.StringType),
        th.Property("category_name", th.StringType),
        th.Property("category_path", th.StringType),
        th.Property("headcategory_id", th.StringType),
        th.Property("headcategory_name", th.StringType),
        th.Property("url", th.StringType),
        th.Property("categories", th.ArrayType(
            th.ArrayType(
                th.ObjectType(
                    th.Property("category", th.StringType),
                    th.Property("category_id", th.StringType),
                    th.Property("level", th.IntegerType)
                )
            )
        )),
        th.Property("variants", th.ArrayType(
            th.ObjectType(

                th.Property("id", th.StringType),
                th.Property("ovalue_id_1", th.StringType),
                th.Property("product_id", th.StringType),
                th.Property("sku", th.StringType),
                th.Property("qty", th.StringType),
                th.Property("price", th.StringType),
                th.Property("weight", th.StringType),
                th.Property("before_price", th.StringType),
                th.Property("purchase_price", th.StringType),
                th.Property("image_id", th.StringType),
                th.Property("gtin", th.StringType),
                th.Property("hidden", th.StringType),
                th.Property("qty_location", th.StringType),
                th.Property("disable_minusqty", th.BooleanType),
                th.Property("values", th.ArrayType(
                    th.ObjectType(
                        th.Property("ovalue_field", th.StringType),
                        th.Property("name", th.StringType),
                        th.Property("val", th.StringType)
                    )
                ))
            )
        ))       
    ).to_dict()
    
    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return None

class OrderStream(hotglueStream):
    """Define custom stream."""
    name = "order"
    path = "/orders"
    records_jsonpath = "$.[*]"
    primary_keys = ["order_id"]
    replication_key = None
    order_page = False
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(

        th.Property("order_id", th.StringType),
        th.Property("date_created", th.StringType),
        th.Property("total_amount", th.StringType),
        th.Property("status", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        if "order_id" in record.keys():
            return {
            "order_parent_id": record["order_id"],
            }
        else:
            return {}
    


class OrderDetailsStream(hotglueStream):
    """Define custom stream."""
    name = "order_details"
    path = "/orders?order_id={order_parent_id}&apps_load=true&include_details=True"
    records_jsonpath = "$.[*]"
    primary_keys = ["order_id"]
    replication_key = None
    parent_stream_type = OrderStream

    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(

        th.Property("order_id", th.StringType),
        th.Property("date_created", th.StringType),
        th.Property("date_paid", th.StringType),
        th.Property("status", th.StringType),
        th.Property("status_date", th.StringType),
        th.Property("items_amount", th.StringType),
        th.Property("shipping_amount", th.StringType),
        th.Property("total_amount", th.StringType),
        th.Property("discount_amount", th.StringType),
        th.Property("total_pay_amount", th.StringType),
        th.Property("tax_amount", th.StringType),
        th.Property("tax_log", th.ObjectType(
            th.Property("products", th.CustomType({"type": ["object"]})),
            th.Property("discount", th.ArrayType(th.CustomType({"type": ["object"]}))),
            th.Property("shipping", th.ArrayType(
                th.ObjectType(
                th.Property("total_amount", th.IntegerType),
                th.Property("tax_amount", th.IntegerType),
                th.Property("percentage", th.IntegerType)
                )
            )),
        th.Property("tax_amount_discount", th.IntegerType),
        th.Property("tax_amount_shipping", th.IntegerType),
        th.Property("tax_amount_total", th.IntegerType),
        th.Property("tax_free", th.BooleanType)
        )),
        th.Property("currency", th.StringType),
        th.Property("total_weight", th.StringType),
        th.Property("shipping_id", th.StringType),
        th.Property("shipping_name", th.StringType),
        th.Property("info_trackingnumber", th.StringType),
        th.Property("info_shipcompany", th.StringType),
        th.Property("is_taxfree", th.BooleanType),    
        th.Property("is_taxontop", th.BooleanType),    
        th.Property("notes", th.StringType),    
        th.Property("refunds", th.ArrayType(th.CustomType({"type": ["object"]}))),    
        th.Property("products", th.ArrayType(
             th.ObjectType(

                 th.Property("id", th.StringType),
                 th.Property("product_id", th.StringType),
                 th.Property("title", th.StringType),
                 th.Property("price", th.StringType),
                 th.Property("qty", th.StringType),
                 th.Property("variant", th.StringType),
                 th.Property("variant_id", th.StringType),
                 th.Property("sku", th.StringType)

             )
        )),    
        th.Property("paymethod", th.ObjectType(
                 th.Property("method", th.StringType),
                 th.Property("transaction_id", th.StringType)
        )),    
        th.Property("metadata", th.StringType)  
    ).to_dict()
    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return None

   